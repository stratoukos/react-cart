var D = React.DOM;

var ProductRow = React.createBackboneClass({
	mixins: [React.BackboneMixin('product')],
	updateQuantity: function() {
		var newQuantity = parseInt(this.refs.quantity.getDOMNode().value, 10);
		this.props.product.set('quantity', newQuantity);
	},
	setActive: function() {
		this.props.product.setActive();
	},
	render: function() {
		var product = this.props.product;

		return D.tr({
				className: product.get('active') ? 'active' : '',
				onClick: this.setActive
			},
			D.td({}, product.get('id')),
			D.td({}, product.get('description')),
			D.td({}, product.get('price')),
			D.td({}, D.input({
				type: 'number',
				ref: 'quantity',
				defaultValue: product.get('quantity')
			})),
			D.td({}, product.totalPrice()),
			D.td({}, D.button({onClick: this.updateQuantity}, 'add'))
		);
	}
});

var ProductTable = React.createBackboneClass({
	mixins: [React.BackboneMixin('catalog')],
	render: function() {
		var catalog = this.props.catalog;

		var rows = catalog.map(function(product) {
			return ProductRow({product: product, key: product.get('id')});
		});

		return D.table({className: 'table'},
			D.tr({},
				D.th({}, 'id'),
				D.th({}, 'description'),
				D.th({}, 'price'),
				D.th({}, 'quantity'),
				D.th({}, 'total price'),
				D.th({}, '')
			),
			D.tbody({}, rows)
		);
	}
});

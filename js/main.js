var catalog = window.catalog = new Catalog();

React.renderComponent(
	ProductTable({catalog: catalog}),
	document.getElementById('products')
);

React.renderComponent(
	Cart({catalog: catalog}),
	document.getElementById('cart')
);

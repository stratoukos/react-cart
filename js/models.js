var Product = Backbone.Model.extend({
	defaults: {
		id: null,
		description: null,
		price: null,
		quantity: 0,
		active: false
	},

	totalPrice: function() {
		return this.get('price') * this.get('quantity');
	},
	setActive: function() {
		if (typeof this.collection.activeProduct !== 'undefined') {
			this.collection.activeProduct.set('active', false);
		}
		this.collection.activeProduct = this;
		this.set('active', true);

		console.log('setting active');
	}
});

var Catalog = Backbone.Collection.extend({
	model: Product,
	initialize: function() {
		$.ajax({
			url: 'db.json',
			success: function(data, status, jqxhr) {
				this.set(data.catalog);
			}.bind(this)
		});
	},

	totalQuantity: function() {
		return this.reduce(function(memo, product) {
			return memo + product.get('quantity');
		}, 0);
	},
	totalPrice: function() {
		return this.reduce(function(memo, product) {
			return memo + product.totalPrice();
		}, 0);
	}
});

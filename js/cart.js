var D = React.DOM;

var CartRow = React.createBackboneClass({
	mixins: [React.BackboneMixin('product')],

	updateQuantity: function(event) {
		var newQuantity = parseInt(event.target.value, 10);
		this.props.product.set('quantity', newQuantity);
	},
	render: function() {
		var product = this.props.product;

		return D.tr({},
			D.td({}, product.get('description')),
			D.td({}, D.input({
				type: 'number',
				value: product.get('quantity'),
				onChange: this.updateQuantity
			}))
		);
	}
});

var Cart = React.createBackboneClass({
	changeOptions: 'add remove reset sort change',
	mixins: [React.BackboneMixin('catalog')],

	getInitialState: function() {
		return {open: false};
	},
	toggleOpen: function(event) {
		event.preventDefault();
		this.setState({open: !this.state.open});
	},
	render: function() {
		var catalog = this.props.catalog;

		var totalQuantity = catalog.totalQuantity();
		var totalPrice = catalog.totalPrice();

		var rows = catalog.filter(function(product) {
			return product.get('quantity') !== 0;
		}).map(function(product) {
			return CartRow({product: product, key: product.get('id')});
		});

		var nodes = D.div({},
			'cart has ', totalQuantity, ' products, which cost ', totalPrice, '€ ',
			D.span({onClick: this.toggleOpen, className: 'toggle'},
				'toggle'
			),
			D.table({className: this.state.open ? 'open' : ''}, rows)
		);
		return nodes;
	}
});
